import os, wmi, shutil, socket

class GsCollector():
    def __init__(self):
        self.teste = 'Ok'
        self.pc = wmi.WMI()
        self.configs = {
                            'os':'',
                            'ram':'', 
                            'hd':'', 
                            'processador':'', 
                            'monitores':[], 
                            'usuario':os.environ['USERNAME'],
                            'nome_computador':socket.gethostname()
                        }
        self.SetOs()
        self.SetProcessador()
        self.SetRam()
        self.SetMonitores()
        self.SetHd()

    def SetOs(self):
        self.configs['os'] = self.pc.Win32_OperatingSystem()[0].Name.split('|')[0]

    def SetProcessador(self):
        self.configs['processador'] = self.pc.Win32_Processor()[0].Name

    def SetRam(self):
        self.configs['ram'] = self.pc.Win32_OperatingSystem()[0].TotalVisibleMemorySize

    def SetMonitores(self):
        pnps = self.pc.Win32_PnPEntity(ConfigManagerErrorCode=0)
        for p in pnps:
            if p.PNPClass == 'Monitor':
                self.configs['monitores'].append(p.Name)

    def SetHd(self):
        total, used, free = shutil.disk_usage("/")
        self.configs['hd'] = total
