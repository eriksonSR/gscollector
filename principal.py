import tkinter as tk
import requests, json
from gscollector import GsCollector
from tkinter import messagebox


class WinPrincipal():
    def __init__(self, gsc):
        self.gsc = gsc
        self.root = tk.Tk()
        self.root.title("GSCollector")
        self.root.grid_columnconfigure((0,1), weight=1)
        self.CriaElementos()
        self.root.mainloop()

    def CriaElementos(self):
        self.lbl_pat_pc = tk.Label(self.root, text="Patrimônio pc:", width=40, anchor='w')
        self.lbl_usuario = tk.Label(self.root, text="Usuário:", width=40, anchor='w')
        self.lbl_os = tk.Label(self.root, text="Sistema Operacional:", width=40, anchor='w')
        self.lbl_processador = tk.Label(self.root, text="Processador:", width=40, anchor='w')
        self.lbl_ram = tk.Label(self.root, text="Ram:", width=40, anchor='w')
        self.lbl_hd = tk.Label(self.root, text="HD", width=40, anchor='w')
        self.lbl_pat_mon1 = tk.Label(self.root, text="Patrimônio monitor um:", width=40, anchor='w')
        self.lbl_pat_mon2 = tk.Label(self.root, text="Patrimônio monitor dois:", width=40, anchor='w')
        self.lbl_monitor1 = tk.Label(self.root, text="Monitor um:", width=40, anchor='w')
        self.lbl_monitor2 = tk.Label(self.root, text="Monitor dois:", width=40, anchor='w')
        self.lbl_obs = tk.Label(self.root, text="Observação:", width=40, anchor='w')

        self.lbl_pat_pc.grid(row=0, column=0, padx=10, pady=5)
        self.lbl_usuario.grid(row=0, column=1)
        self.lbl_os.grid(row=2, column=0, padx=10, pady=5)
        self.lbl_processador.grid(row=2, column=1)
        self.lbl_ram.grid(row=4, column=0, padx=10, pady=5)
        self.lbl_hd.grid(row=4, column=1)
        self.lbl_pat_mon1.grid(row=6, column=0, pady=5)
        self.lbl_pat_mon2.grid(row=6, column=1)
        self.lbl_monitor1.grid(row=8, column=0, pady=5)
        self.lbl_monitor2.grid(row=8 ,column=1)
        self.lbl_obs.grid(row=10, column=0, pady=5)

        self.ent_pat_pc = tk.Entry(self.root, width=45)
        self.ent_pat_pc.insert(0, self.gsc.configs['nome_computador'])
        self.ent_usuario = tk.Entry(self.root, width=45)
        self.ent_usuario.insert(0, self.gsc.configs['usuario'])
        self.ent_os = tk.Entry(self.root, width=45)
        self.ent_os.insert(0, self.gsc.configs['os'])
        self.ent_processador = tk.Entry(self.root, width=45)
        self.ent_processador.insert(0, self.gsc.configs['processador'])
        self.ent_ram = tk.Entry(self.root, width=45)
        self.ent_ram.insert(0, self.gsc.configs['ram'])
        self.ent_hd = tk.Entry(self.root, width=45)
        self.ent_hd.insert(0, self.gsc.configs['hd'])
        self.ent_pat_mon1 = tk.Entry(self.root, width=45)
        self.ent_pat_mon2 = tk.Entry(self.root, width=45)
        self.ent_mon1 = tk.Entry(self.root, width=45)
        if self.gsc.configs['monitores'][0]:
            self.ent_mon1.insert(0, self.gsc.configs['monitores'][0])
        self.ent_mon2 = tk.Entry(self.root, width=45)
        if self.gsc.configs['monitores'][0]:
            self.ent_mon2.insert(0, self.gsc.configs['monitores'][1])
        self.text_obs = tk.Text(self.root, height=2, width=70)

        self.ent_pat_pc.grid(row=1,column=0)
        self.ent_usuario.grid(row=1,column=1)
        self.ent_os.grid(row=3,column=0)
        self.ent_processador.grid(row=3,column=1)
        self.ent_ram.grid(row=5,column=0)
        self.ent_hd.grid(row=5,column=1)
        self.ent_pat_mon1.grid(row=7,column=0)
        self.ent_pat_mon2.grid(row=7,column=1)
        self.ent_mon1.grid(row=9,column=0)
        self.ent_mon2.grid(row=9,column=1)
        self.text_obs.grid(row=11,column=0, columnspan=2, rowspan=2, pady=5)

        self.bntSalvar = tk.Button(text="Salvar", width=45)
        self.bntSalvar["command"] = self.SalvarColeta
        self.bntSalvar.grid(row=13,column=0, pady=5)

    def SalvarColeta(self):
        url_api = 'http://praga/api/v1/gscollector/'
        r = requests.post(url_api, data=json.dumps(self.gsc.configs))
        r = json.loads(r.text)
        if r['status'] == 'sucesso':
            messagebox.showinfo("OK", r['msg'])
        else:
            messagebox.showerror("Erro", r['msg'])

if __name__ == '__main__':
    WinPrincipal(GsCollector())
